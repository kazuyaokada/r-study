---
title: "Graphics 第5章"
author: "Isojima Sakura"
date: "2020/11/9"
output: html_document
editor_options: 
  chunk_output_type: console
---
# 5章 散布図

## 5.0 準備
```{r}
library(ggplot2)
library(gcookbook)
library(ggrepel)
library(dplyr)
library(tidyverse)
```

## 5.1 基本的な散布図を作成する
```{r}
ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point()
```

shapeで点の形状，sizeでサイズを変更できる．

(デフォルトはshape=19，size=2)
```{r}
ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point(shape = 21, size = 1)
```

## 5.2 色と形を使用してデータポイントをグループ化する

グループ化したい変数をエステティック属性にマッピングする．

1つの変数を両方にマッピングすることも，複数の変数をそれぞれにマッピングすることもできる．

手動で形や色を設定したい場合は以下を用いて指定する．

 - 形：scale_shape_manual()
 - 色：scale_colour_brewer()やscale_colour_manual()
```{r}
ggplot(heightweight, aes(x = ageYear, y = heightIn, shape = sex, colour = sex)) +
  geom_point(size = 3) +
  scale_shape_manual(values = c(1, 2)) +
  scale_color_manual(values = c("#d7191c", "#fdae61"))
```

## 5.3 点の形を指定する

すべての点の形を一括で指定したい場合は，geom_point()で指定する．
```{r}
ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point(shape = 3, size = 3)
```

点の形と塗りつぶしの色とで，別々の変数の情報を表現することもできる．
```{r}
# 100ポンド以上の体重であるかどうかでカテゴリ分けする列を追加．
hw <- heightweight %>%
  mutate(weightgroup = ifelse(weightLb < 100,  "< 100", ">= 100"))

ggplot(hw, aes(x = ageYear, y = heightIn, shape = sex, fill = weightgroup)) +
  geom_point(size = 2.5) +
  scale_shape_manual(values = c(21, 24)) +
  scale_fill_manual(
    values = c(NA, "black"),
    guide = guide_legend(override.aes = list(shape = 21))
  )
```

勿論colourにも使える．

ちなみに，guide = guide_legend(override.aes = list(shape = 21))を抜くと凡例がアカンくなる．
```{r}
ggplot(hw, aes(x = ageYear, y = heightIn, shape = sex, colour = weightgroup)) +
  geom_point(size = 2.5) +
  scale_shape_manual(values = c(21, 24)) +
  scale_colour_manual(
    values = c("red", "black")
  )
```

## 5.4 連続値変数を色やサイズにマッピングする

## 5.5 オーバープロットを扱う

大きなデータセットを扱う場合，点がお互いに重なり合って見えづらくなり，
分布を正確に把握しづらくなることがある．
これをオーバープロットという．
```{r}
ggplot(diamonds, aes(x = carat, y = price)) +
  geom_point() # 密です
```

点を半透明にするとやや見やすくなる．
```{r}
ggplot(diamonds, aes(x = carat, y = price)) +
  geom_point(alpha = .1) # 90%

ggplot(diamonds, aes(x = carat, y = price)) +
  geom_point(alpha = .01) # 99%
```

### データをビンに詰め込んで可視化

長方形のビンの中にデータポイントを詰め込み，点の密度を色にマッピングする方法もある．
```{r}
ggplot(diamonds, aes(x = carat, y = price)) +
  stat_bin2d()
```

 - デフォルトのビンは30×30 → 50×50に変更
 - scale_fill_gradient()でグラデーションのハイローを指定
 - デフォルトの凡例に0は表示されないため，limitsを使って手動で範囲を設定
```{r}
ggplot(diamonds, aes(x = carat, y = price)) +
  stat_bin2d(bins = 50) +
  scale_fill_gradient(low = "lightblue", high = "red", limits = c(0, 6000))
```

ほかに，六角形のビンにデータを詰め込むものもある．割愛．

ジッターも割愛．


### 箱ひげ図

1つの軸が離散値，もう1つの軸が連続値の場合，箱ひげ図が適している．

Timeはデフォルトでは数値型の変数として扱われるため，
離散値としてグループ分けするには指定が必要．
```{r}
ggplot(ChickWeight, aes(x = Time, y = weight)) +
  geom_boxplot(aes(group = Time)) +
  geom_jitter(height = 0)
```

## 5.6 回帰モデルの直線をフィットさせる

### 線形回帰モデル

線形回帰モデルの直線を追加するには，stat_smooth(method = lm)で
線形モデルをデータに適用させる．

デフォルトの信頼区間は95％だが，levelで設定，se=FALSEで無効にできる．
```{r}
ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point() +
  stat_smooth(method = lm, level = 0.99) # 99%信頼区間

ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point() +
  stat_smooth(method = lm, se = FALSE) # 信頼区間なし
```

また，今まで同様に，色や線の太さも設定できる．

### LOESS(局所加重多項式)曲線

デフォルトではLOESSが使用される．method=loessを指定してもよい．
```{r}
ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point(colour = "grey60") +
  stat_smooth()
```

### 散布図の点がグループ化されている場合

各ファクタのレベルごとにフィットした線が1つ描かれる．

データの範囲までしか線が描かれないことに注意．
```{r}
ggplot(heightweight, aes(x = ageYear, y = heightIn, colour = sex)) +
  geom_point() +
  geom_smooth()
```

データを外挿して線を描きたいときは，lm()のように外挿可能なモデルを使い，
stat_smooth()のオプションとしてfullrange=TRUEを指定する必要がある．
```{r}
ggplot(heightweight, aes(x = ageYear, y = heightIn, colour = sex)) +
  geom_point() +
  geom_smooth(method = lm, se = FALSE, fullrange = TRUE)
```

geom_smooth()とstat_smooth()の違いについては，

"Use stat_smooth() if you want to display the results with a non-standard geom."

とのこと．

ロジスティック回帰については割愛．

## 5.7 既存のモデルをフィットさせる

独自に作成したモデルをグラフに表示したい場合
```{r}
# モデルの作成
model <- lm(heightIn ~ ageYear + I(ageYear^2), heightweight)

# ageYear列を持つデータフレームを作成し，範囲内で内挿
xmin <- min(heightweight$ageYear)
xmax <- max(heightweight$ageYear)
predicted <- data.frame(ageYear = seq(xmin, xmax, length.out = 100))

# モデルを用いてheightInの予測値を計算
predicted$heightIn <- predict(model, predicted)
head(predicted)

# プロット
ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point(colour = "grey40") +
  geom_line(data = predicted, size = 1)
```

モデルから線を追加する作業は，次の関数で簡略化できる．

（モデルを渡すと，予測に使用している変数名を見つけて，
予測に使用したデータ＋予測された値を含んだデータフレームを返す関数）
```{r}
predictvals <- function(model, xvar, yvar, xrange = NULL, samples = 100, ...){
  if (is.null(xrange)){
    if (any(class(model) %in% c("lm", "glm")))
      xrange <- range(model$model[[xvar]])
    else if (any(class(model) %in% "loess"))
      xrange <- range(model$x)
  }
  newdata <- data.frame(x = seq(xrange[1], xrange[2], length.out = samples))
  names(newdata) <- xvar
  newdata[[yvar]] <- predict(model, newdata = newdata, ...)
  newdata
}
```

使用例
```{r}
modlinear <- lm(heightIn ~ ageYear, heightweight)
modloess <- loess(heightIn ~ ageYear, heightweight)

lm_predicted <- predictvals(modlinear, "ageYear", "heightIn")
loess_predicted <- predictvals(modloess, "ageYear", "heightIn")

ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point(colour = "grey40") +
  geom_line(data = lm_predicted, colour = "red", size = .8) +
  geom_line(data = loess_predicted, colour = "blue", size = .8)
```

## 5.8 複数の既存のモデルをフィットさせる

predictvals()関数をgroup_by()やdo()と併せて使用する．

```{r}
# モデルオブジェクトのリストを作成
models <- heightweight %>%
  group_by(sex) %>%
  do(model = lm(heightIn ~ ageYear, .)) %>%
  ungroup()
models
models$model

# 各モデルの予測値を得る
predvals <- models %>%
  group_by(sex) %>%
  do(predictvals(.$model[[1]], xvar = "ageYear", yvar = "heightIn"))

# プロット
ggplot(heightweight, aes(x = ageYear, y = heightIn, colour = sex)) +
  geom_point() +
  geom_line(data = predvals)

# グループ分けにファセットを使用した場合
ggplot(heightweight, aes(x = ageYear, y = heightIn, colour = sex, shape = sex)) +
  geom_point() +
  geom_line(data = predvals) +
  facet_grid(. ~ sex)
```

予測値の線のx軸の範囲を同じにしたい場合は，predictvals()内でxrangeを指定する（割愛）．

## 5.9 注釈とモデルの係数を追加する

annotate()を使って手動でテキストを追加．
```{r}
# 線形モデルを作成
model <- lm(heightIn ~ ageYear, heightweight)
summary(model) # R-squaredを確認

# 予測データを作成
pred <- predictvals(model, "ageYear", "heightIn")

# Rの数式表現の文法で数式を入力する場合，parse=TRUEを指定
ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point() +
  geom_line(data = pred) +
  annotate("text", x = 16.5, y = 52, label = "r^2 == 0.42", parse = TRUE)
```

### 自動的に数式を組み立てさせる場合

文字列を作成して，その文字列を解析し，正しい数式を生成する．
```{r}
eqn <- sprintf(
  "italic(y) == %.3g + %.3g * italic(x) * ',' ~~ italic(r)^2 ~ '=' ~ %.2g",
  coef(model)[1],
  coef(model)[2],
  summary(model)$r.squared
)
ggplot(heightweight, aes(x = ageYear, y = heightIn)) +
  geom_point() +
  geom_line(data = pred) +
  annotate(
    "text", 
    x = Inf, y = -Inf,
    label = eqn, parse = TRUE,
    hjust = 1.1, vjust = -.5
    )
```

## 5.10 散布図の縁にラグを表示する

geom_rug()を使う．
```{r}
ggplot(faithful, aes(x = eruptions, y = waiting)) +
  geom_point() +
  geom_rug()
```

## 5.11 散布図の点にラベルを付ける

手動で注釈を追加するには，annotate()を使用して座標とラベルを指定する．
```{r}
# データを絞っておく
countries_sub <- countries %>%
  filter(Year == 2009 & healthexp > 2000)

ggplot(countries_sub, aes(x = healthexp, y = infmortality)) +
  geom_point() +
  annotate("text", x = 4350, y = 5.4, label = "Canada") +
  annotate("text", x = 7400, y = 6.8, label = "USA")
```

データに対して自動的にラベルを追加するには，geom_text()を使用して，
labelエステティック属性にマッピングする．
```{r}
ggplot(countries_sub, aes(x = healthexp, y = infmortality)) +
  geom_point() +
  geom_text(aes(label = Name), size = 4)
```

点のラベルが重ならないように自動的に調整するには，geom_text_repel()または
geom_label_repel()を使用する．
```{r}
ggplot(countries_sub, aes(x = healthexp, y = infmortality)) +
  geom_point() +
  geom_text_repel(aes(label = Name), size = 3)

ggplot(countries_sub, aes(x = healthexp, y = infmortality)) +
  geom_point() +
  geom_label_repel(aes(label = Name), size = 3)
```

### 各ラベルの位置を完全に制御したい場合

annotate()やgeom_text()を使う．

geom_text()ではデフォルトで注釈がx座標とy座標の中心に置かれるが，
vjustで垂直方向，hjustで水平方向にずらせる．

また，x,yの値を増減させることもできる．
```{r}
# vjust=0でベースラインを点と同じ位置に
# hjust=0でラベルを右に調整し，さらにxの値を調整する
ggplot(countries_sub, aes(x = healthexp, y = infmortality)) +
  geom_point() +
  geom_text(
    aes(x = healthexp + 100, label = Name),
    size = 3,
    vjust = 1,
    hjust = 0
  )
```

その他にもラベルの位置調整方法はあるが，細かくなるので割愛．

## 5.12 バルーンプロットを作成する

geom_point()をscale_size_area()と合わせて使う．

デフォルトではscale_size_continuous()が使われ，半径にマッピングされるが，
半径が2倍＝面積は4倍であるため，ふさわしくない．
```{r}
# データの絞り込み
countrylist <- c("Canada", "Ireland", "United Kingdom", "United States", "New Zealand", 
                 "Iceland", "Japan", "Luxembourg", "Netherlands", "Switzerland")
cdat <- countries %>%
  filter(Year == 2009, Name %in% countrylist)

# 円を大きくして，GDPを面積にマッピング
ggplot(cdat, aes(x = healthexp, y = infmortality, size = GDP)) +
  geom_point(shape = 21, colour = "black", fill = "cornsilk") +
  scale_size_area(max_size = 15)
```


## 5.13 散布図の行列を作成する

pairs()を使用する．
```{r}
# データを絞る
c2009 <- countries %>%
  filter(Year == 2009) %>%
  select(Name, GDP, laborrate, healthexp, infmortality)
c2009_num <- select(c2009, -Name)

# プロット
pairs(c2009_num)
```

線形回帰の直線や，LOESSの曲線など，関数を定義すれば，
行列のパネルに対してカスタマイズした関数を使うこともできる（割愛）．