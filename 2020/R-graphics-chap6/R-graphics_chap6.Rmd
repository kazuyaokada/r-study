---
title: "graphics_chap6"
author: "Syu Sakai"
date: "2020/11/16"
output:
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
    number_sections: false
editor_options: 
  chunk_output_type: console
---

```{r include=FALSE}
library(gcookbook)
library(tidyverse)
library(MASS)
library(plyr)
```

# 6章 データ分布の要約

## 6-1 基本的なヒストグラムの作成

geom_histogram()を使用する．

```{r}
ggplot(faithful,aes(x = waiting)) +
  geom_histogram()
```

データフレーム形式になっていないデータからヒストグラムを作る場合は，データフレームにNULLを指定し，ggplot()にデータ値のベクトルを渡す．  
このやり方は後述の密度曲線などほかのグラフ作成の際にも使用できる．

```{r}
w <- faithful$waiting #値をベクトルに収納
ggplot(NULL, aes(x = w)) +
  geom_histogram()
```

binwidthを使うことで，データを分割するビン数を変更できる(デフォルト値30)．

```{r}
#ビン幅を5に設定
ggplot(faithful, aes(x = waiting)) +
  geom_histogram(binwidth = 5, fill = "white", colour = "black")
```

```{r}
#xの分布範囲を15のビンに分割
binsize <- diff(range(faithful$waiting))/15
 #range()で最大値と最小値を返す→→→diff()で最大値と最小値の差を返す→→→任意の数値で割りビンのサイズを求める
ggplot(faithful, aes(x = waiting)) +
  geom_histogram(binwidth = binsize, fill = "white", colour = "black")
```

## 6-2 グループ化されたデータから複数のヒストグラムを作成

facet_grid()を用い，グループをファセット変数としてプロットする．

```{r}
ggplot(birthwt, aes(x = bwt)) +
  geom_histogram(fill = "white", colour = "black") +
  facet_grid(smoke ~ .)
```

```{r}
birthwt1 <- birthwt #データのコピーの作成
birthwt1$smoke <- factor(birthwt1$smoke) #smokeをファクタに変換する
levels(birthwt1$smoke)
birthwt1$smoke <- revalue(birthwt1$smoke, c("0" = "No Smoke", "1" = "Smoke")) #ファクタレベルの変更
ggplot(birthwt1, aes(x = bwt)) +
  geom_histogram(fill = "white", colour = "black") +
  facet_grid(smoke ~.) 
# 「~.」を右につけるか，「.~」を左につけるかで，データが左右に並ぶか上下に並ぶか変えられる．
```

各グループのサンプルサイズが異なる場合，scales = "free"を用いるとグループ間でのデータの比較が行いやすくなる．

```{r}
birthwt2 <- birthwt #ラベル変更
birthwt2$race <- birthwt2$race 
birthwt2$race <- factor(birthwt2$race)
levels(birthwt2$race)
birthwt2$race <- revalue(birthwt2$race, c("1" = "white", "2" = "black", "3" = "other"))

ggplot(birthwt2, aes(x = bwt)) +
  geom_histogram(fill = "white", colour = "black") +
  facet_grid(race ~ ., scales = "free")
```

fillにグループ変数をマッピングすることでも複数のヒストグラムを作成することができる．

```{r}
birthwt1$smoke <- factor(birthwt1$smoke) #smokeをファクタに変換する
ggplot(birthwt1, aes(x = bwt, fill = smoke)) +
  geom_histogram(position = "identity", alpha = 0.4) #smokeをfillにマッピングし，position = "identity"で積み上げでない棒グラフを設定し，alphaで色を半透明にする．
```

## 6-3 密度曲線の作成

geom_density()を使うことで，カーネル密度曲線を作成できる．

```{r}
ggplot(faithful,aes(x = waiting)) +
  geom_density()
```

adjustのパラメータ値を設定すると平滑化帯域幅を変更でき，帯域幅が広いほど曲線は滑らかになる．

```{r}
# adjustをそれぞれ0.25(赤),デフォルトの1(黒),2(青)に設定した密度曲線
ggplot(faithful,aes(x = waiting)) +
  geom_density(adjust = .25, colour = "red") +
  geom_density() +
  geom_density(adjust = 2, colour = "blue")
```

xの範囲を指定することで曲線を端まで表示することができる．

```{r}
ggplot(faithful,aes(x = waiting)) +
  geom_density(fill = "blue", alpha = .2) +
  xlim(35,105)
```

## 6-4 グループ化されたデータから複数の密度曲線を作成

6-2と同様，facet_grid()を用いる．

```{r}
birthwt1 <- birthwt #データのコピーの作成
birthwt1$smoke <- factor(birthwt1$smoke) #smokeをファクタに変換する
levels(birthwt1$smoke)
birthwt1$smoke <- revalue(birthwt1$smoke, c("0" = "No Smoke", "1" = "Smoke")) #ファクタレベルの変更
birthwt1
ggplot(birthwt1, aes(x = bwt)) +
  geom_density(fill = "white", colour = "black") +
  facet_grid(smoke ~.)
```

## 6-5 頻度の折れ線グラフの作成

geom_freqpoly()を用いる．

```{r}
ggplot(faithful,aes(x = waiting)) +
  geom_freqpoly()
```

ヒストグラムと同様にビン幅をコントロールすることも可能．(省略)

## 6-6 基本的な箱ひげ図の作成

geom_boxplot()を用いる．

```{r}
ggplot(birthwt, aes(x = factor(race), y = bwt)) +
  geom_boxplot()
```

外れ値が多くオーバープロットになる場合，外れ値の点のサイズと種類をoutlier.sizeとoutlier.shapeで変更できる(デフォルトでそれぞれ2,16)

```{r}
ggplot(birthwt, aes(x = factor(race), y = bwt)) + 
  geom_boxplot(outlier.size = 1.5, outlier.shape = 21)
```

グループが1つだけの箱ひげ図を抽出する場合，xに任意の値を与える必要がある．

```{r}
ggplot(birthwt, aes(x = 1, y = bwt)) +　#xの値を1に設定
  geom_boxplot() +
  scale_x_continuous(breaks = NULL) +  #x軸のメモリを削除
  theme(axis.title.x = element_blank())　#x軸のラベルを削除
```

## 6-7 ノッチを付けた箱ひげ図の作成

geom_boxplot()でnotch = TRUEとすることで箱ひげ図にノッチ(中央値を表す)を付けられる．

```{r}
ggplot(birthwt, aes(factor(race), y = bwt)) +
  geom_boxplot(notch = TRUE)　#くびれ部が95%信頼区間を表す
```

## 6-8 箱ひげ図に平均値を追加する

stat_summary()を使うことで箱ひげ図に平均値をプロットできる．

```{r}
ggplot(birthwt, aes(x = factor(race), y = bwt)) +
  geom_boxplot()　+
  stat_summary(fun = "mean", geom = "point", shape = 23, size = 3, fill = "white")
```

## 6-9 バイオリンプロットの作成

geom_violin()を用いる．

```{r}
p <- ggplot(heightweight, aes(x = sex, y = heightIn))
p + geom_violin()
```

バイオリンプロットはカーネル密度曲線を左右対称に描いたもの．
慣例として中央値に白い点を打った幅の狭い箱ひげ図が重ね書きされる．

```{r}
p + geom_violin() +
  geom_boxplot(width = .1, fill = "black", outlier.colour = NA) +
  stat_summary(fun = median, geom = "point", fill = "white", shape = 21, size = 2.5)
```

## 6-10 (ウィルキンソンの)ドットプロットの作成

geom_dotplot()を用いる．

```{r}
countries2009 <- subset(countries, Year == 2009 & healthexp > 2000)
p <- ggplot(countries2009, aes(x = infmortality))
p + geom_dotplot()
```

積み上げられたドットはx軸上に等間隔に配置されていない．  
等間隔に配置するには，method = "histodot"を用いる．

```{r}
p + geom_dotplot(method = "histodot", binwidth = .25) +
  scale_y_continuous(breaks = NULL) + #y軸目盛りを除く
  theme(axis.title.y = element_blank()) #y軸ラベルを除く
```

## 6-11 グループ化されたデータから複数のドットプロットを作成

binaxis = "y"を設定することで，ビンをy軸上に区切り，ドットをx軸方向に積み上げることができる．

```{r}
ggplot(heightweight, aes(x = sex, y = heightIn))  +
  geom_dotplot(method = "histodot", binaxis = "y", binwidth = .5, stackdir = "center") #stackdirによりドットをy軸上にセンタリング
```

ドットプロットは箱ひげ図に重ね書きされることがある．(省略)

## 6-12 2次元データから密度プロットを作成

stat_density2d()を用いることでデータから二次元のカーネル密度推定を計算できる．

```{r}
#散布図と密度の等高線を作成する．
p <- ggplot(faithful, aes(x = eruptions, y = waiting))
p + geom_point() +
  stat_density2d(aes(colour = ..level..)) #..level..により密度曲線の高さを等高線の色にマッピングする．
```

二次元カーネル密度推定の可視化にはデフォルトで等高線が使われるが，ほかにもタイルを使った可視化が可能．

```{r}
#密度を塗りつぶしの色にマッピング
p + stat_density2d(aes(fill = ..density..), geom = "raster", contour = FALSE) #contour = FALSEにより等高線を非表示にする．

#散布図に重ね書き．密度推定値を透過率にマッピング
p + geom_point() +
  stat_density2d(aes(alpha = ..density..), geom = "tile", contour = FALSE)
```















