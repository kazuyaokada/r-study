---
title: "2019年R勉強会：#2 第5章・第6章"
author: "Farah Elida"
date: "2019/10/10"
output: 
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
    number_sections: false
editor_options: 
  chunk_output_type: console
---

# 第5章　関数定義とプログラミング入門
自作の関数を定義する方法と簡単なプログラムを書く方法紹介する.

# 関数(5-1)

関数という形で呼び出すことができる.
```{r}
date()

#引数という形で関数に与える
x <- 2.71828182845
log(x)


#引数が2個以上ある場合はカンマ(,)で区切って並べる
x <- 10
log(x^2, base=10)   #底を指定する引数baseに10を指定して対数を計算

x <- 10
log10(x^2)
```

# 関数定義の基本(5-2)
(1)関数名を決める

(2)入力する変数(の個数や種類)を指定する

(3)計算手順を1行ずつ記述する

(4)関数return()で計算結果を出力する


基本的な形式:


関数名 <- function(引数/入力値) {

  <計算処理の1行目>
  
  <計算処理の2行目>
  
  .....
  
  return(計算結果を返す)
  
}

```{r}
#引数がない関数
myfunc01 <- function() {
 return(1)
}

myfunc01()

#引数がある関数
myfunc02 <- function(x) {
 return(x)
}

myfunc02(2)

#数値を2倍したものの定義
mydouble <- function(x) {
 return(2*x)    
}

mydouble(5)

#引数は2個以上の場合
mytimes <- function(x,y) {
  return(x*y)   
}

mytimes(3,4)

#ベクトルの場合
vect <- function(x) (y <- 2*x)      #return(2*x)と同じ
vect(c(1, 2))  

```

## 練習問題①
### (1)引数として数値を入れると,入力した数値を二乗したものを結果として返す関数mypower01()を定義せよ.
```{r}
mypower01 <- function(x) {
  return(x^2)
}

mypower01(3)

mypower01 <- function(x) return(x^2) #1行の場合,{}使わない
mypower01(3)
```

### (2)引数として２つの数値xのy乗を結果として返す関数mypower02()を定義せよ.

```{r}
mypower02 <- function(x,y) {
  return(x^y)
}

mypower02(3,4)

mypower02 <- function(x,y) return(x^y) #1行の場合,{}使わない
mypower02(3,4)

```

### (3)関数の中では行を複数書いてもよい.例えば,数値を入力すると数値の平方根を結果として返す関数mysqrt()を定義する.

```{r}
#返す関数に定義する
mysqrt <- function(x) {
  return(sqrt(x))
}

mysqrt(2.7182818)

#複数の行を使う
mysqrt <- function(x) {
  y <- sqrt(x)
  return(y)
}

mysqrt(2.7182818)
```

以上を参考に数値を入力すると平方根の対数をとったものを結果として返す関数mysqrtlog()を定義せよ.

```{r}
mysqrtlog <- function(x) {
  return(log(sqrt(x)))
}

mysqrtlog(2.7182818)

#読みやすい場合
mysqrtlog <- function(x) {
  y <- sqrt(x)
  return(log(y))
}

mysqrtlog(2.7182818)

```

# プログラムの基本 (5-3)
## 条件分岐: if,else 

ある条件で場合分けをして処理を行いたい.


基本的な形式:

if(条件式) {

  <条件式がTRUEのときに実行されるしき>
  
} else　{

  <条件式がFALSEのときに実行される式>
  
}

```{r}
#入力がxが負ならば正しにして出力し,負でなければそのまま出力する
myabs <- function(x) {
  if(x<0){   #xが0未満ならば条件式はTRUE(真)となる
  return(-x)
}
else { 　　　#xが0以上ならば条件式はFALSE(偽)となる
  return(x) 
  }
}

myabs(-2)
myabs(3)

#表5.1(比較演算子): xが1等しいならば
myabs1 <- function(x) {
  if(x==1){
  return(x)
}
else {
  return("Null")
}
}

myabs1(-2)
myabs(1)

#表5.1: xが2.0でなければ
myabs2 <- function(x) {
  if(x!=2.0){    #xが2.0でなければ,xを返す
  return(x)
}
else {
  return(20)
}
}

myabs2(2.0)
myabs2(3.1)

#表5.1: xが3以上ならば
myabs3 <- function(x) {
  if(x>=3.0){
  return(x)
}
else {
  return("y")
}
}

myabs3(2.0)
myabs3(3.1)

#表5.1: xが4.0未満ならば
myabs4 <- function(x) {
  if(x<4.0){
  return("a")
}
else {
  return("b")
}
}

myabs4(3.8)
myabs4(5.2)
```

## 練習問題②

###(1)引数xが1より大きい場合のみ1を出力する関数myone()を定義せよ.

```{r}
myone <- function(x) {
  if(x>1) return(1)
}

myone(1.1)
myone(0)
```

###(2)引数xが1より大きい場合は1を出力し,引数xが1より大きくない場合は0を出力する関数myindex()を定義せよ.

```{r}
myindex <- function(x) {
  if(x>1){
  return(x)
}
else {
  return(0)
}
}

myindex(2.2)
myindex(0.5)
```

###(3)２つの数値a,bを入力した数値の差を結果として返す関数mydistance()を定義せよ.ただし,差は正の数であるものとする(つまり|a-b|を計算する必要がある).

```{r}
mydistance <- function(a,b) {
  if((a-b)>0) {
  return(a-b)
}
else {
  return(b-a)
}
}

mydistance(4,3)
mydistance(3,4)

#シンプル書き方
mydistance <- function(a,b) {
  if(a>b) return(a-b)
  else    return(b-a)
}

mydistance(4,3)
mydistance(3,4)

```

## 繰り返し文: for
ある処理を繰り返して実行したい場合.通常は数列c(1,2,3)や1:5などを指定する.

基本的な形式:

for (ループ変数inリスト)　{

  < 繰り返す式>
  
}

```{r}
#0が入っている変数aに5回だけ1を足す場合
myloop <- function() {
  a <- 0             #aに0を代入
  for (i in 1:5) {   #1から5までの5回繰り返す
    a <- a+1         #aに1を足すを繰り返す
  }
  return(a)
}

myloop()

#リストは3:7に置き換える
myloop2 <- function() {
  a <- 0
  for (i in 3:7) {  
    a <- a+1
  }
  return(a)
}

myloop2()

#整数値nを入れると1からnまでの和を返す関数
mysum <- function(n) {
  i <- 0
  for (j in 1:n) {  
    i <- i+j   
  }
  return(i)
}

mysum(5)

```

##練習問題③

###(1)以下の#####部分に「変数xにiを足す」を5回繰り返す処理を追記せよ.

x <- 0

for (i in 1:5) #####

x

```{r}
x <- 0
for (i in 1:5) x <- x+i  #xにiを足す
x

```

###(2)以下の#####部分に「ベクトルxにiをくっつける」を5回繰り返す処理を追記せよ.

x <- c()

for (i in 1:5) #####

x

```{r}
x <- c()                   #空のベクトルを用意
for (i in 1:5) x <- c(x,i) #xにiをくっ付ける
x

```

###(3)「1からxまでの間の偶数をすべて足し合わせる」という関数myeven()を定義せよ.

```{r}
myeven <- function(x){
  a <- 0
  for (i in 1:x) {
    if(i%%2 == 0) a <- a+i #iを2で割った余り,%%は剰余
  }
return(a)
}

myeven(10)

```
###(4)NAは欠損値(データなし)を表す.ベクトル x<-c(1,2,3,NA,4,5,NA)　について,　NAは足さずにほかの要素だけを足し算するような関数myplus()を定義せよ.

ヒント:

-　ベクトルxの要素の数(大きさ,長さ)
を求める関数length(x)をもちればよい.

-　数値aがNAかどうかを判定する関数is.na(a)をもちればよい

```{r}
#ヒントを使う場合
x <- c(1, 2, 3, NA, 4, 5, NA)
myplus <- function(x) {
  i <- 0
  for(j in 1:length(x)) {  
    if(!is.na(x[j])) i <- i+x[j] #x[j]の値がNAでなけならば
  }
  return(i)
}

myplus(x)

#ヒントを使う場合
x <- c(1, 2, 3, NA, 4, 5, NA)
myplus <- function(x) {
  x[is.na(x)] <- 0 
  i <- 0
  for (j in 1:length(x)) i <- i+x[j]
  return(i)
}

myplus(x)

#for,if elseを使わない場合
x <- c(1, 2, 3, NA, 4, 5, NA)
x[is.na(x)] <- 0  #NAは0に置き換え
x
sum(x)


#for,if elseを使わない場合
x <- c(1, 2, 3, NA, 4, 5, NA)
sum(x,na.rm=TRUE) #na.rmは引数,xがNAでなれば


```

#落穂ひろい(5-4)

(1)関数定義の式が1行しかない場合や,ifやelse,for文の後の式が1行しかない場合は,｛｝で囲まず.

```{r}
myfunc <- function(x) return(2*x)
myfunc(5)

myabs <- function(x) {
  if(x<0) return(-x)
  else return(x)
}
myabs(-5)

myfunc <- function(x) return(2*x)
myfunc(5)

#また,ifelseはシンプルに示すことができる
myabs <- function(x) 
  ifelse(x<0, x <- "TRUE", x <- "FALSE")
myabs(4)

```

(2)返す関数return()を書いて出力の値を指定するのが普通だが,出力する値をそのまま書くことでも値が同様に出力される.

```{r}
myfunc <- function(x) {
  2*x    #return(2*xと同じ)
}
myfunc(5)
```

(3)1つの値について複数の条件する場合は,表5.2(論理演算子)を用いる.

!: NOT (でない)
&&: AND(かつ)
||: OR(または)

```{r}
#&&の例
myfunc04 <- function(x) {
  if((-2<x) && (x<2)) return(x)
}
myfunc04(1)

myfunc04 <- function(x) {
  if(c(1,2,3) && (c(2,2,4))) return(x)
}
myfunc04(c(1,2,3))


```

1つの値ではなく複数の要素を持ったベクトルについて複数の条件を指定する場合は,表5.3(論理演算子)を用いる.

!: NOT (でない)
&: AND(かつ)
|: OR(または)

```{r}
#&の例
((-2:2) >= 0) & ((-2:2) <= 0)  #ベクトルを返す

#&&の例
((-2:2) >= 0) && ((-2:2) <= 0)　#最初の要素のみ判定する

```

(4)3つ以上の条件分岐する場合は,elseifを用いる.

if (条件式1) {

  < 条件式1がTRUEのときに実行される式>
  
}

else if (条件式2)

  <条件式1がFALSEで条件式2がTRUEのときに実行される式>
  
}

else {

  <条件式1も条件式2もFALSEのときに実行される式>
  
}

```{r}
myfunc <- function(x) {
  if (x<0)                  return(-x)
  else if ((0<=x) && (x<5)) return(5)
  else                      return(x)
}
myfunc(6)
```

(5) ()を用いると,代入と表示を同時に行える.

```{r}
x <- 1:4      #代入のみ結果は表示なし

(x <- 1:4)    #代入した結果を表示

myfunc1 <- function(x) y <-2*x
myfunc1(1:4)  # 何も表示されない

myfunc2 <- function(x) (y <-2*x)
myfunc2(1:4)  # 代入した結果を表示

```

(6) 関数を定義するとき,1つの命令が数行にわたる場合が多い.Rの画面に直接入力すると,間違ったときにもう一度始めから入力し直さなければ不便.そのときに,適当なエディタ(メモ帳)など)に命令を書く.それをRの画面にコピー&ペストするのがよい.

